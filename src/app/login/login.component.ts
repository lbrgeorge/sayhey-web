import { UserService } from './../providers/user.service';
import { Component, OnInit } from '@angular/core';
import * as swal from 'sweetalert';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public login_user = {
    username: "",
    password: ""
  };

  public register_user = {
    email: "",
    username: "",
    password: ""
  };

  public lineClass = "line-anim";

  constructor(private userService: UserService) { }

  ngOnInit() {
    setTimeout(() => {
      this.lineClass = "line-anim anim-complete";
    }, 1000);
  }

  doLogin() {
    if (this.login_user.username.trim().length > 0 && this.login_user.password.trim().length > 0)
    {
      this.userService.login(this.login_user.username, this.login_user.password)
        .then((result: Boolean) => {
          if (result == true)
          {
            location.reload();
          }
          else {
            swal("Autenticação", "Username/password invalid!", "error");
          }
        });
    }
  }

}
