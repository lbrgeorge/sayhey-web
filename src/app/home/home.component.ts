import { SocketService, GroupMessage } from './../providers/socket.service';
import { Message } from './../models/Message.model';
import { Group } from './../models/Group.model';
import { UserService } from './../providers/user.service';
import { User } from './../models/User.model';
import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import * as $ from 'jquery';
import * as moment from 'moment';
import * as swal from 'sweetalert';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewChecked {
  @ViewChild('chatBox') private chatBox: ElementRef;

  public localUser: User;
  public groups: Group[] = [];
  public unread_messsages: GroupMessage[] = [];

  public currentGroup: Group;
  private replyMessage: string = "";

  //Socket events
  private onMessageReceive;

  constructor(private userService: UserService, private socketService: SocketService) { 
    this.localUser = this.userService.getLocalUser();

    if (this.localUser != undefined)
    {
      this.groups = this.localUser.Groups;

      //Receive message handler
      this.onMessageReceive = (data: GroupMessage) => {
        if (this.currentGroup == undefined || data.group.ID != this.currentGroup.ID)
        {
          this.unread_messsages.push(data);
        }

        /*if (this.currentGroup != undefined)
        {
          this.currentGroup.Messages.push(data.message);
        }*/
      };
    }
  }

  ngOnInit() {
    this.socketService.initialize();
    this.socketService.onMessageReceive.on(this.onMessageReceive);

    $(".heading-compose").click(() => {
      $(".side-two").css({
        "left": "0"
      });
    });

    $(".newMessage-back").click(() => {
      $(".side-two").css({
        "left": "-100%"
      });
    });
  }

  ngAfterViewChecked() {        
    this.scrollChatBox();
  }

  doLogout(ev: any) {
    ev.preventDefault();

    this.userService.logout();
    window.location.reload();
  }

  GetGroupLastMessage(group: Group): Message {
    if (group.Messages.length > 0)
    {
      let messages = group.Messages;

      messages.sort((a: Message, b: Message) => {
        if (a.Date < b.Date)
        {
          return -1;
        }

        if (a.Date > b.Date)
        {
          return 1;
        }

        return 0;
      });

      return messages[0];
    }

    return undefined;
  }
  
  GetGroupNextMessage(msg: Message, group: Group) {
    let messages = group.Messages;
    
    if (messages.length < 1) return undefined;
    else {
      messages = messages.reverse();

      let index = messages.findIndex(p => p.ID == msg.ID);
      
      if (messages[index - 1] == undefined) return msg;
      
      return messages[index - 1];
    }
  }

  GetGroupUnreadMessages(group: Group) {
    return this.unread_messsages.filter(p => p.group.ID == group.ID);
  }

  GetLastMessageTime(group: Group) {
    let msg = this.GetGroupLastMessage(group);

    if (msg == undefined) return "";
    else {
      let date = moment(msg.Date);

      return date.format("HH:mm");
    }
  }

  GetGroupMembersNames() {
    let str = "";

    if (this.currentGroup != undefined)
    {
      str = "Você";

      for(let member of this.currentGroup.Members)
      {
        if (member.ID != this.localUser.ID) str += ", " + member.Username;
      }
    }

    return str;
  }

  userColor(user: User) {
    return {"color": "#fc1010"};
  }

  showGroupChat(group: Group) {
    this.currentGroup = group;
    
    for(let i = this.unread_messsages.length - 1; i >= 0 ; i--)
    {
      console.log(this.unread_messsages[i]);
      if (this.unread_messsages[i].group.ID == group.ID)
      {
        this.unread_messsages.splice(i, 1);
      }
    }
  }
  
  replyBox(ev: any) {
    if (ev.which == 13)
    {
      ev.preventDefault();
      this.sendMessage();
    }
  }

  sendMessage() {
    if (this.replyMessage.trim().length > 0)
    {
      let msg = new Message(
        0,
        this.replyMessage,
        "",
        this.localUser
      );

      this.socketService.sendMessage(msg, this.currentGroup);

      this.replyMessage = "";
    }
  }

  leaveGroup(ev: any) {
    ev.preventDefault();

    if (this.currentGroup != undefined)
    {
      swal({
        title: "Leave group",
        text: "Do you really want to leave this group?",
        buttons: {
          cancel: {
            text: "No, I want stay",
            value: false,
            visible: true,
            closeModal: true,
            className: ""
          },
          confirm: {
            text: "Yes, I want leave now!",
            value: true,
            visible: true,
            closeModal: false,
            className: ""
          }
        }
      }).then(response => {
        if (response == true)
        {
          let index = this.groups.findIndex(p => p.ID == this.currentGroup.ID);
          
          if (index >= 0)
          {
            this.groups.splice(index, 1);
            this.currentGroup = undefined;
          }
        }
      });
    }
  }

  scrollChatBox() {
    if (this.currentGroup != undefined)
    {
      this.chatBox.nativeElement.scrollTop = this.chatBox.nativeElement.scrollHeight;
    }
  }

}
