import { User } from './User.model';

export class Message {
    /**
     *
     */
    constructor(
        public ID: number,
        public Text: string,
        public Date: string,
        public User: User
    ) {}
}