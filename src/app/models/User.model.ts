import { Group } from './Group.model';

export class User {
    /**
     *
     */
    constructor(
        public ID: number,
        public Username: string,
        public Email: string,
        public Avatar: string,
        public Token: string,
        public Groups?: Group[]
    ) {}
}