import { User } from './User.model';
import { Message } from './Message.model';

export class Group {
    /**
     *
     */
    constructor(
        public ID: number,
        public Name: string,
        public Topic: string,
        public Messages: Message[],
        public Members: User[]
    ) {}
}