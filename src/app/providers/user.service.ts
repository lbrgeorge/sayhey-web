import { Group } from './../models/Group.model';
import { User } from './../models/User.model';
import { ApiService } from './api.service';
import { DatabaseService } from './database.service';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  private localUser: User;

  constructor(private databaseService: DatabaseService, private apiService: ApiService) { }

  login(username: string, password: string): Promise<Boolean> {
    this.apiService.prepare("username", username);
    this.apiService.prepare("password", password);

    return this.apiService.post("?f=user&a=login")
      .then((result: any) => {
        if (result.Error.length > 0) return false;
        else {
          this.databaseService.setUserAuthentication(result.user_id, result.user_token);

          this.apiService.prepare("user_id", username);
          this.apiService.prepare("user_token", password);

          return this.apiService.post("?f=user&a=get")
            .then((r: any) => {
              if (r.Error.length > 0) return false;
              else {
                this.localUser = r.User as User;
                return true;
              }
            })
        }
      })
  }

  logout() {
    this.databaseService.removeUserAuthentication();
  }

  getInfo(): Promise<Boolean> {
    let auth = this.databaseService.getUserAuthentication();

    this.apiService.prepare("user_id", auth.user_id);
    this.apiService.prepare("user_token", auth.user_token);

    return this.apiService.post("?f=user&a=get")
      .then((result: any) => {
        if (result.Error.length > 0) return false;
        else {
          this.localUser = result.User as User;
          return true;
        }
      })
  }

  /** METHODS */

  getLocalUser() {
    return this.localUser;
  }

  getGroups() {
    return this.localUser.Groups;
  }

  saveGroup(group: Group) {
    if (group != undefined)
    {
      let idx = this.localUser.Groups.findIndex(p => p.ID == group.ID);

      if (idx >= 0)
      {
        this.localUser.Groups[idx] = group;
      }
    }
  }
}
