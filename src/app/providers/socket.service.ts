import { Group } from './../models/Group.model';
import { Message } from './../models/Message.model';
import { UserService } from './user.service';
import { User } from './../models/User.model';
import { Injectable } from '@angular/core';
import * as socketIO from 'socket.io-client';

export interface GroupMessage {
  message: Message,
  group: Group
}

interface ILiteEvent<T> {
  on(handler: { (data?: T): void }) : void;
  off(handler: { (data?: T): void }) : void;
}

class LiteEvent<T> implements ILiteEvent<T> {
  private handlers: { (data?: T): void; }[] = [];

  public on(handler: { (data?: T): void }) : void {
      this.handlers.push(handler);
  }

  public off(handler: { (data?: T): void }) : void {
      this.handlers = this.handlers.filter(h => h !== handler);
  }

  public trigger(data?: T) {
      this.handlers.slice(0).forEach(h => h(data));
  }

  public expose() : ILiteEvent<T> {
      return this;
  }
}

@Injectable()
export class SocketService {

  private hostAddress = "http://chat.lbr-gang.com:5192";

  private localUser: User;
  private socket: any;
  private connected: Boolean = false;

  /* EVENTS */
  public readonly messageReceive = new LiteEvent<GroupMessage>();
  public get onMessageReceive() { return this.messageReceive.expose(); };

  constructor(private userService: UserService) { 
    this.localUser = this.userService.getLocalUser();  
  }

  initialize() {
    this.localUser = this.userService.getLocalUser(); //Update user

    if (this.localUser != undefined)
    {
      this.socket = socketIO.connect(this.hostAddress, {transports: ['websocket'], query: "userid=" + this.localUser.ID + "&token=" + this.localUser.Token});

      this.socket.on('connect', () => {
        console.log("Socket connected!");

        this.socket.emit("setupUser", {
          ID: this.localUser.ID,
          Username: this.localUser.Username,
          Groups: this.localUser.Groups
        });

        this.connected = true;
      });

      this.socket.on('error', (err) => {
        console.log("Socket Error: " + err);
        this.connected = false;
      });

      this.socket.on('reconnect_error', (err) => {
        this.connected = false;
      });

      this.socket.on('connect_timeout', () => {
        console.log("Socket connection timeout.");
        this.connected = false;
      });

      this.socket.on('disconnect', () => {
        console.log("Socket disconnected!");
        this.connected = false;
      });

      this.socket.on('userGroupMessage', (data) => {
        
        let group = this.userService.getGroups().find(p => p.ID == data.GroupID);

        if (group != undefined)
        {
          let user = group.Members.find(p => p.ID == data.UserID);

          let msg = new Message(
            data.MessageID,
            data.Message,
            data.Date,
            user
          );

          group.Messages.push(msg);

          this.userService.saveGroup(group);
          this.messageReceive.trigger({
            message: msg,
            group: group
          });
        }
        else console.log("Lost message: " + data);
      });
    }
  }

  sendMessage(message: Message, group: Group) {
    this.socket.emit("newMessage", {
      UserID: message.User.ID,
      Username: message.User.Username,
      UserAvatar: message.User.Avatar,
      GroupID: group.ID,
      Message: message.Text,
      MessageSpecial: false
    });
  }

}
