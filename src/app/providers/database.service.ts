import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

export interface UserAuth {
  user_id: number,
  user_token: string
}

@Injectable()
export class DatabaseService {

  constructor(private localStorageService: LocalStorageService) { }

  getUserAuthentication(): UserAuth {
    let user_id = this.localStorageService.get("user_id") as number;
    let user_token = this.localStorageService.get("user_token") as string;

    return {user_id: user_id, user_token: user_token};
  }

  setUserAuthentication(user_id: number, user_token: string) {
    this.localStorageService.set("user_id", user_id);
    this.localStorageService.set("user_token", user_token);
  }

  removeUserAuthentication() {
    this.localStorageService.remove("user_id", "user_token");
  }

}
