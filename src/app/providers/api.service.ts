import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

export interface ApiError {
  Completed: Boolean;
  Error: string;
}

@Injectable()
export class ApiService {

  public baseUri: string;
  private jsonType: Headers;

  private body_data: any = {};
  private body_json: any;

  constructor(public http: Http) {
    this.baseUri = "http://chat.lbr-gang.com/v2/api/";

    this.jsonType = new Headers();
    this.jsonType.append("Content-Type", "application/json");
  }

  prepare(key: any, value: any) {
    this.body_data[key] = value;
    this.body_json = JSON.stringify(this.body_data);
  }

  post(path: string): Promise<any> {
    return this.http.post(this.baseUri + path, this.body_json, {headers: this.jsonType})
      .toPromise()
      .then((response: any) => {
        let result = response.json();

        return result;
      },
      (error) => {
        console.log("ApiPost Error: " + JSON.stringify(error));
        return {Error: error.name};
      })
  }

  get(path: string): Promise<any> {
    return this.http.get(this.baseUri + path)
      .toPromise()
      .then((response: any) => {
        let result = response.json();

        return result;
      },
      (error) => {
        console.log("ApiGet Error: " + JSON.stringify(error));
        return {Error: error.name};
      })
  }

}
