import { UserService } from './providers/user.service';
import { User } from './models/User.model';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public localUser: User;

  constructor(private userService: UserService) {
    this.userService.getInfo()
      .then((result: Boolean) => {
        if (result == true) this.localUser = this.userService.getLocalUser();
      });
  }
}
